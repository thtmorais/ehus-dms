<?php

namespace app\models;

use Yii;

class Manutencao extends \yii\db\ActiveRecord
{

    public $transiction = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manutencao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7', 'q8', 'q9', 'q10', 'q11a', 'q11b', 'q11c', 'q11d', 'q11e', 'q11f', 'q11g', 'q11h', 'q11i', 'q12', 'q13', 'q14', 'q15', 'q17a', 'q17b', 'q17c', 'q17d', 'q17e', 'q17f', 'q17g', 'q17h', 'q17i', 'q17j', 'q18', 'q19', 'q20', 'q22', 'q23', 'q24', 'q25', 'q26', 'q27', 'q28', 'q29', 'q30', 'transiction', 'entrevistado_id'], 'required'],
            [['q1', 'q2', 'q3', 'q5', 'q6', 'q7', 'q8', 'q9', 'q10', 'q11a', 'q11b', 'q11c', 'q11d', 'q11e', 'q11f', 'q11g', 'q11h', 'q11i', 'q12', 'q13', 'q14', 'q15', 'q16', 'q17a', 'q17b', 'q17c', 'q17d', 'q17e', 'q17f', 'q17g', 'q17h', 'q17i', 'q17j', 'q19', 'q20', 'q21', 'q22', 'q23', 'q24', 'q25', 'q26', 'q27', 'q28', 'q29', 'entrevistado_id'], 'integer'],
            [['q30', 'q4'], 'string'],
            [['q18'], 'safe'],
            [['entrevistado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entrevistado::className(), 'targetAttribute' => ['entrevistado_id' => 'id']],        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'q1' => 'Questão 1: Em relação à afirmação “A água é um bem cada vez mais escasso”, você:',
            'q2' => 'Questão 2: Na escala abaixo, qual é a importância que você atribui às medidas de economia de água?',
            'q3' => 'Questão 3: Existe um sistema de abastecimento de água em caso de emergência?',
            'q4' => 'Questão 4 (Deixe em branco se não souber): O reservatório de água do hospital possui capacidade aproximada de quantos litros?',
            'q5' => 'Questão 5: Em caso de corte no abastecimento de água, por quanto tempo o reservatório conseguiria abastecer o hospital?',
            'q6' => 'Questão 6: O sistema de abastecimento de água obedece à legislação sanitária?',
            'q7' => 'Questão 7: A água chega com a pressão adequada até as torneiras, chuveiros e outros equipamentos?',
            'q8' => 'Questão 8: Como você classifica o atual estado de conservação do hospital como um todo?',
            'q9' => 'Questão 9: Como você classifica o atual estado de conservação da estrutura hidráulica do hospital?',
            'q10' => 'Questão 10: Na sua visão, há desperdício de água no hospital?',
            'q11a' => 'Vaso sanitário',
			'q11b' => 'Mictório',
			'q11c' => 'Torneiras',
			'q11d' => 'Chuveiros/duchas',
			'q11e' => 'Bebedouros',
			'q11f' => 'Tubulação interna',
			'q11g' => 'Tubulação externa',
			'q11h' => 'Registros/válvulas',
			'q11i' => 'Outros equipamentos',
            'q12' => 'Questão 12: Com qual frequência o hospital apresenta problemas de infiltração por causa de vazamentos?',
            'q13' => 'Questão 13: O hospital tem algum sistema de reaproveitamento de água?',
            'q14' => 'Questão 14: Na escala abaixo, como você considera a eficiência do hospital no trato com a água?',
            'q15' => 'Questão 15: Existe um programa de manutenção preventiva no hospital?',
            'q16' => 'Questão 16(não obrigatória): Se sim, você considera esta manutenção preventiva:',
            'q17a' => 'Vaso sanitário',
			'q17b' => 'Mictório',
			'q17c' => 'Lavatórios',
			'q17d' => 'Pia de cozinha',
			'q17e' => 'Chuveiros/duchas',
			'q17f' => 'Bebedouros',
			'q17g' => 'Tubulação interna',
			'q17h' => 'Tubulação externa',
			'q17i' => 'Registros',
			'q17j' => 'Outros equipamentos',
            'q18' => 'Questão 18: Quais são as dificuldades enfrentadas pelo trabalho de manutenção?',
            'q19' => 'Questão 19: O número de profissionais que trabalham na manutenção hidráulica é:',
            'q20' => 'Questão 20: O hospital ou a secretaria de saúde promove ações de educação ambiental voltadas ao uso racional da água?',
            'q21' => 'Questão 21(não obrigatória): Se sim, como você avalia estas ações de educação ambiental:',
            'q22' => 'Questão 22: Qual é a natureza do seu vínculo com o hospital?',
            'q23' => 'Questão 23: Quantas horas por semana você trabalha?',
            'q24' => 'Questão 24: Escolaridade',
            'q25' => 'Questao 25: Sexo',
            'q26' => 'Questao 26: Cor ou raça',
            'q27' => 'Questao 27: Em qual faixa de renda mensal você se enquadra?',
			'q28' => 'Questão 28: Há quantos anos trabalha no hospital/clínica autal?',
			'q29' => 'Questão 29: Você tem quantos anos de experiência de trabalho na área da saúde?',
            'q30' => 'Questao 30: Deixe sugestões que você daria para melhorar o uso da água na sua unidade de saúde.',
            'entrevistado_id' => 'Entrevistado ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntrevistado()
    {
        return $this->hasOne(Entrevistado::className(), ['id' => 'entrevistado_id']);
    }
}
