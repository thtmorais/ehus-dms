<?php

namespace app\models;

use Yii;

class Cozinha extends \yii\db\ActiveRecord
{

    public $transiction = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cozinha';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['q1', 'q2', 'q3', 'q5', 'q6', 'q7', 'q8', 'q9a', 'q9b', 'q9c', 'q9d', 'q9e', 'q9f', 'q9g', 'q9h', 'q9i', 'q10', 'q11', 'q12', 'q13', 'q14', 'q15', 'q17a', 'q17b', 'q17c', 'q17d', 'q17e', 'q17f', 'q17g', 'q17h', 'q17i', 'q18', 'q20', 'q21', 'q22', 'q23', 'q24', 'q25', 'q26', 'q27', 'q28', 'transiction', 'entrevistado_id'], 'required'],
            [['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7', 'q8', 'q9a', 'q9b', 'q9c', 'q9d', 'q9e', 'q9f', 'q9g', 'q9h', 'q9i', 'q10', 'q11', 'q12', 'q13', 'q14', 'q15', 'q16', 'q17a', 'q17b', 'q17c', 'q17d', 'q17e', 'q17f', 'q17g', 'q17h', 'q17i', 'q18', 'q19', 'q20', 'q21', 'q23', 'q24', 'q25', 'q26', 'q27', 'entrevistado_id'], 'integer'],
            [['q22', 'q28'], 'string'],
            [['entrevistado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entrevistado::className(), 'targetAttribute' => ['entrevistado_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'q1' => 'Questão 1: Em relação à afirmação “A água é um bem cada vez mais escasso”, você:',
            'q2' => 'Questão 2: Na escala abaixo, qual é a importância que você atribui às medidas de economia de água?',
            'q3' => 'Questão 3: Existe um sistema de abastecimento de água que atenda a cozinha em caso de emergência (falta de água)?',
            'q4' => 'Questão 4(não obrigatória): Se sim, em caso de corte no abastecimento de água, por quanto tempo o reservatório conseguiria abastecer a cozinha?',
            'q5' => 'Questão 5: O sistema de abastecimento de água obedece à legislação sanitária?',
            'q6' => 'Questão 6: O sistema de escoamento da água, depois que ela sai da cozinha, obedece à legislação sanitária?',
            'q7' => 'Questão 7: Como você classifica o atual estado de conservação da estrutura hidráulica da cozinha?',
            'q8' => 'Questão 8: Na sua visão, há desperdício de água na cozinha?',
            'q9a' => 'Vaso sanitário:',
            'q9b' => 'Torneiras:',
            'q9c' => 'Bebedouros:',
            'q9d' => 'Tubulação interna:',
            'q9e' => 'Tubulação externa:',
            'q9f' => 'Registros/válvulas:',
            'q9g' => 'Panelas elétricas:',
			'q9h' => 'Lavadoura de louças',
			'q9i' => 'Outros',
            'q10' => 'Questão 10: Com qual frequência a cozinha apresenta problemas de infiltração por causa de vazamento?',
            'q11' => 'Questão 11: As torneiras da cozinha têm sistema de abertura e fechamento adequado?',
            'q12' => 'Questão 12: As torneiras têm vazão:',
            'q13' => 'Questão 13: A cozinha tem algum sistema de reaproveitamento de água?',
            'q14' => 'Questão 14: Na escala abaixo, como você considera a eficiência da cozinha no trato com a água?',
			'q15' => 'Questão 15: Existe um programa de manutenção preventiva na cozinha?',
			'q16' => 'Questão 16(não obrigatória): Se sim, você considera esta manutenção preventiva:',
			'q17a' => 'Vaso sanitário',
            'q17b' => 'Torneiras',
            'q17c' => 'Bebedouros',
            'q17d' => 'Tubulação interna',
            'q17e' => 'Tubulação externa',
            'q17f' => 'Registros/válvulas',
			'q17g' => 'Panelas elétricas',
			'q17h' => 'Lavadoura de louças',
            'q17i' => 'Outros equipamentos',
            'q18' => 'Questão 18: O hospital ou a secretaria de saúde promove ações de educação ambiental voltadas ao uso racional da água?',
            'q19' => 'Questão 19(não obrigatória): Se sim, como você avalias estas ações de educação ambiental:',
            'q20' => 'Questão 20: Qual é a natureza do seu vínculo com o hospital?',
            'q21' => 'Questão 21: Quantas horas por semana você trabalha?',
            'q22' => 'Questão 22: Escolaridade:',
            'q23' => 'Questão 23: Sexo:',
            'q24' => 'Questão 24: Cor ou raça:',
            'q25' => 'Questao 25: Qual é sua renda mensal?',
            'q26' => 'Questao 26: Há quanto tempo trabalha no hospital/clínica?',
            'q27' => 'Questao 27: Há quanto tempo trabalha na área da saúde?',
            'q28' => 'Questão 28: Deixe sugestões que você daria para melhorar o uso da água na cozinha.',
            'entrevistado_id' => 'Entrevistado ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntrevistado()
    {
        return $this->hasOne(Entrevistado::className(), ['id' => 'entrevistado_id']);
    }
}
