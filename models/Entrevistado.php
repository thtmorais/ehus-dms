<?php

namespace app\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "entrevistado".
 *
 * @property string $email
 * @property integer $categoria
 * @property integer $id
 *
 * @property BanhoTecnico $banhoTecnico
 */
class Entrevistado extends \yii\db\ActiveRecord
{

    public $aceite;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entrevistado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'categoria', 'aceite'], 'required'],
            [['categoria'], 'integer'],
            [['aceite'], 'boolean'],
            [['email'], 'string', 'max' => 100],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'categoria' => 'Categoria',
            'id' => 'ID',
            'aceite' => 'Aceito os Termos & Condições',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanhoTecnico()
    {
        return $this->hasOne(BanhoTecnico::className(), ['entrevistado_id' => 'id']);
    }
}
