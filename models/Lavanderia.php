<?php

namespace app\models;

use Yii;

class Lavanderia extends \yii\db\ActiveRecord
{

    public $transiction = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lavanderia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['q1', 'q2', 'q3', 'q5', 'q6', 'q7', 'q8', 'q9a', 'q9b', 'q9c', 'q9d', 'q9e', 'q9f', 'q9g', 'q9h', 'q10', 'q11', 'q12', 'q14a', 'q14b', 'q14c', 'q14d', 'q14e', 'q14f', 'q14g', 'q14h', 'q14i', 'q14j', 'q14k', 'q15', 'q17', 'q18', 'q19', 'q20', 'q21', 'q22', 'q23', 'q24', 'q25', 'transiction', 'entrevistado_id'], 'required'],
            [['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7', 'q8', 'q9a', 'q9b', 'q9c', 'q9d', 'q9e', 'q9f', 'q9g', 'q9h', 'q10', 'q11', 'q12', 'q13', 'q14a', 'q14b', 'q14c', 'q14d', 'q14e', 'q14f', 'q14g', 'q14h', 'q14i', 'q14j', 'q14k', 'q15', 'q16', 'q17', 'q18', 'q19', 'q21', 'q22', 'q23', 'q24', 'entrevistado_id'], 'integer'],
            [['q20', 'q25'], 'string'],
            [['entrevistado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entrevistado::className(), 'targetAttribute' => ['entrevistado_id' => 'id']],        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'q1' => 'Questão 1: Em relação à afirmação “A água é um bem cada vez mais escasso”, você:',
            'q2' => 'Questão 2: Na escala abaixo, qual é a importância que você atribui às medidas de economia de água?',
            'q3' => 'Questão 3:Existe um sistema de abastecimento de água que atenda a lavanderia em caso de emergência (falta de água)?  ',
            'q4' => 'Questão 4 (não obrigatória): Se sim, em caso de corte no abastecimento de água, por quanto tempo o reservatório conseguiria abastecer a lavanderia?',
            'q5' => 'Questão 5: O sistema de abastecimento de água da lavanderia obedece à legislação sanitária?',
            'q6' => 'Questão 6: O sistema de escoamento da água, depois que ela sai da lavanderia, obedece à legislação sanitária',
            'q7' => 'Questão 7: Como você classifica o atual estado de conservação da estrutura hidráulica da lavanderia?',
            'q8' => 'Questão 8: Na sua visão, há desperdício de água na lavanderia?',
            'q9a' => 'Vaso sanitário:',
            'q9b' => 'Torneiras:',
            'q9c' => 'Bebedouros:',
            'q9d' => 'Tubulação interna:',
            'q9e' => 'Tubulação externa:',
            'q9f' => 'Registros/válvulas:',
            'q9g' => 'Lavadouras:',
			'q9h' => 'Outros',
            'q10' => 'Questão 10: Com qual frequência o lavanderia apresenta problemas de infiltração por causa de vazamentos?',
            'q11' => 'Questão 11: Na escala abaixo, como você considera a eficiência da lavanderia no trato com a água?',
            'q12' => 'Questão 12: Existe um programa de manutenção preventiva na lavanderia?',
            'q13' => 'Questão 13 (não obrigatória): Se sim, você considera esta manutenção preventiva:',
            'q14a' => 'Vaso sanitário',
            'q14b' => 'Torneiras',
            'q14c' => 'Bebedouros',
            'q14d' => 'Tubulação interna',
            'q14e' => 'Tubulação externa',
            'q14f' => 'Lavadouras',
			'q14g' => 'Centrifugas',
			'q14h' => 'Calandras',
            'q14i' => 'Secadoras',
			'q14j' => 'Registros',
            'q14k' => 'Outros equipamentos',
            'q15' => 'Questão 15: O hospital ou a secretaria de saúde promove ações de educação ambiental voltadas ao uso racional da água?',
            'q16' => 'Questão 16 (não obrigatória): Se sim, como você avalia estas ações de educação ambiental:',
            'q17' => 'Questão 17: Qual é a natureza do seu vínculo com o hospital?',
            'q18' => 'Questão 18: Quantas horas por semana você trabalha?',
            'q19' => 'Questão 19: Escolaridade: ',
            'q20' => 'Questão 20: Sexo:',
            'q21' => 'Questão 21: Cor ou raça:',
            'q22' => 'Questao 22: Em qual faixa de renda mensal você se enquadra?',
            'q23' => 'Questao 23: Há quantos anos trabalha no hospital/clínica atual?',
            'q24' => 'Questao 24: Você tem quantos anos de experiência de trabalho na área da saúde?',
            'q25' => 'Questao 25: Deixe suas sugestões para melhorar o uso da água na lavanderia.',
            'entrevistado_id' => 'Entrevistado ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntrevistado()
    {
        return $this->hasOne(Entrevistado::className(), ['id' => 'entrevistado_id']);
    }
}
