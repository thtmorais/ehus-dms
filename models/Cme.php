<?php

namespace app\models;

use Yii;

class Cme extends \yii\db\ActiveRecord
{

    public $transiction = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['q1', 'q2', 'q3', 'q5', 'q6', 'q7', 'q8', 'q9', 'q10a', 'q10b', 'q10c', 'q10d', 'q10e', 'q10f', 'q10g', 'q10h', 'q10i', 'q11', 'q12', 'q13', 'q15a', 'q15b', 'q15c', 'q15d', 'q15e', 'q15f', 'q15g', 'q15h', 'q15i', 'q16', 'q18', 'q19', 'q20', 'q21', 'q22', 'q23', 'q24', 'q25', 'q26', 'transiction', 'entrevistado_id'], 'required'],
            [['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7', 'q8', 'q9', 'q10a', 'q10b', 'q10c', 'q10d', 'q10e', 'q10f', 'q10g', 'q10h', 'q10i', 'q11', 'q12', 'q13', 'q14', 'q15a', 'q15b', 'q15c', 'q15d', 'q15e', 'q15f', 'q15g', 'q15h', 'q15i', 'q16', 'q17', 'q18', 'q19', 'q20', 'q21', 'q22', 'q23', 'q24', 'q25', 'entrevistado_id'], 'integer'],
            [['q26'], 'string'],
            [['entrevistado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entrevistado::className(), 'targetAttribute' => ['entrevistado_id' => 'id']],        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'q1' => 'Questão 1: Em relação à afirmação “A água é um bem cada vez mais escasso”, você:',
            'q2' => 'Questão 2: Na escala abaixo, qual é a importância que você atribui às medidas de economia de água?',
            'q3' => 'Questão 3: Existe um sistema de abastecimento de água que atenda o CME em caso de emergência (falta de água)?',
            'q4' => 'Questão 4(não obrigatória): Se sim, em caso de corte no abastecimento de água, por quanto tempo o reservatório conseguiria abastecer o CME?',
            'q5' => 'Questão 5: O sistema de abastecimento de água do CME obedece à legislação sanitária?',
            'q6' => 'Questão 6: O sistema de escoamento da água, depois que ela sai do CME, obedece à legislação sanitária?',
            'q7' => 'Questão 7: Como você avalia a qualidade da água utilizada no CME?',
            'q8' => 'Questão 8: Como você classifica o atual estado de conservação da estrutura hidráulica do CME?',
            'q9' => 'Questão 9: Na sua visão, há desperdício de água no CME?',
            'q10a' => 'Vaso sanitário:',
            'q10b' => 'Torneiras:',
            'q10c' => 'Bebedouros:',
            'q10d' => 'Tubulação interna:',
            'q10e' => 'Tubulação externa:',
            'q10f' => 'Registros/válvulas:',
            'q10g' => 'Autoclaves:',
			'q10h' => 'Ar condicionado',
			'q10i' => 'Outros',
            'q11' => 'Questão 11: Com qual frequência o CME apresenta problemas de infiltração por causa de vazamentos?',
            'q12' => 'Questão 12: Na escala abaixo, como você considera a eficiência do CME no trato com a água?',
            'q13' => 'Questão 13: Existe um programa de manutenção preventiva no CME?',
            'q14' => 'Questão 14(não obrigatória): Se sim, você considera esta manutenção preventiva:',
            'q15a' => 'Vaso sanitário',
            'q15b' => 'Torneiras',
            'q15c' => 'Bebedouros',
            'q15d' => 'Tubulação interna',
            'q15e' => 'Tubulação externa',
            'q15f' => 'Registros/válvulas',
			'q15g' => 'Autoclaves',
			'q15h' => 'Ar condicionado',
            'q15i' => 'Outros equipamentos',
            'q16' => 'Questão 16: O hospital ou a secretaria de saúde promove ações de educação ambiental voltadas ao uso racional da água?',
            'q17' => 'Questão 17(não obrigatória): Se sim, como você avalia estas ações de educação ambiental:',
            'q18' => 'Questão 18: Qual é a natureza do seu vínculo com o hospital?',
            'q19' => 'Questão 19: Quantas horas por semana você trabalha?',
            'q20' => 'Questão 20: Escolaridade: ',
            'q21' => 'Questão 21: Sexo:',
            'q22' => 'Questão 22: Cor ou raça:',
            'q23' => 'Questao 23: Em qual faixa de renda mensal você se enquadra?',
            'q24' => 'Questao 24: Há quantos anos trabalha no hospital/clínica atual?',
            'q25' => 'Questao 25: Você tem quantos anos de experiência de trabalho na área da saúde?',
            'q26' => 'Questão 26: Deixe sugstões que você daria para melhorar o uso de água no CME.',
            'entrevistado_id' => 'Entrevistado ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntrevistado()
    {
        return $this->hasOne(Entrevistado::className(), ['id' => 'entrevistado_id']);
    }
}
