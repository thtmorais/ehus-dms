<?php

namespace app\models;

use Yii;

class BanhoTecnico extends \yii\db\ActiveRecord
{

    public $transiction = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banho_tecnico';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['q1', 'q2', 'q3', 'q5', 'q6', 'q7', 'q8', 'q9a', 'q9b', 'q9c', 'q9d', 'q9e', 'q9f', 'q9g', 'q10', 'q11', 'q12', 'q14a', 'q14b', 'q14c', 'q14d', 'q14e', 'q14f', 'q14g', 'q15', 'q17', 'q18', 'q19', 'q20', 'q21', 'q22', 'q23', 'q24', 'q25', 'entrevistado_id', 'transiction'], 'required'],
            [['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7', 'q8', 'q9a', 'q9b', 'q9c', 'q9d', 'q9e', 'q9f', 'q9g', 'q10', 'q11', 'q12', 'q13', 'q14a', 'q14b', 'q14c', 'q14d', 'q14e', 'q14f', 'q14g', 'q15', 'q16', 'q17', 'q18', 'q19', 'q20', 'q21', 'q22', 'q23', 'q24', 'entrevistado_id'], 'integer'],
            [['q25'], 'string'],
            [['entrevistado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entrevistado::className(), 'targetAttribute' => ['entrevistado_id' => 'id']],       ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'q1' => 'Questão 1: Em relação à afirmação “A água é um bem cada vez mais escasso”, você:',
            'q2' => 'Questão 2: Na escala abaixo, qual é a importância que você atribui às medidas de economia de água?',
            'q3' => 'Questão 3: Existe um  sistema  de  abastecimento  que  atenda o  banho  técnico  em  caso  de emergência (falta de água)? ',
            'q4' => 'Questão 4(não obrigatória): Se sim, em caso de corte no abastecimento de água, por quanto tempo o reservatório consegueria fornecer água para manter o banho técnico?',
            'q5' => 'Questão 5: O sistema de abastecimento de água obedece à legislação sanitária?',
            'q6' => 'Questão 6: O sistema de escoamento da água, depois que ela sai do banho técnico, obedece à legislação sanitária?',
            'q7' => 'Questão 7: Como você classifica o atual estado de conservação da estrutura hidráulica dos banheiros onde são realizados os banhos técnicos?',
            'q8' => 'Questão 8: Na sua visão, há desperdício de água nos banheiros?',
            'q9a' => 'Vaso sanitário:',
            'q9b' => 'Torneiras:',
            'q9c' => 'Chuveiros/Duchas:',
            'q9d' => 'Tubulação interna:',
            'q9e' => 'Tubulação externa:',
            'q9f' => 'Registros/Válvulas:',
            'q9g' => 'Outros',
            'q10' => 'Questão 10: Com qual frequência os banheiros apresentam problemas de infiltração por causa de vazamento?',
            'q11' => 'Questão 11: Na escala abaixo, como você considera a eficiência dos banheiros no trato com a água?',
            'q12' => 'Questão 12: Existe um programa de manutenção preventiva nos banheiros?',
            'q13' => 'Questão 13(não obrigatória): Se sim, você considera esta manutenção preventiva:',
            'q14a' => 'Vaso sanitário:',
            'q14b' => 'Torneiras:',
            'q14c' => 'Chuveiros/Duchas:',
            'q14d' => 'Tubulação interna:',
            'q14e' => 'Tubulação externa:',
            'q14f' => 'Registros/Válvulas:',
            'q14g' => 'Outros equipamentos',
            'q15' => 'Questão 15: O hospital ou a secretaria de saúde promove ações de educação ambiental voltadas ao uso racional da água?',
            'q16' => 'Questão 16(não obrigatória): Se sim, como você avalias estas ações de educação ambiental:',
            'q17' => 'Questão 17: Qual é a natureza do seu vínculo com o hospital?',
            'q18' => 'Questão 18: Quantas horas por semana você trabalha?',
            'q19' => 'Questão 19: Escolaridade: ',
            'q20' => 'Questão 20: Sexo:',
            'q21' => 'Questão 21: Cor ou raça:',
            'q22' => 'Questao 22: Em qual faixa de renda mensal você se enquadra?',
            'q23' => 'Questao 23: Há quantos anos você trabalha no hospital/clínica atual?',
            'q24' => 'Questao 24: Você tem quantos anos de experiência de trabalho na área da saúde?',
            'q25' => 'Questao 25: Deixe sugestões que você daria para melhorar o uso da água nos banheiros.',
            'entrevistado_id' => 'entrevistado ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntrevistado()
    {
        return $this->hasOne(Entrevistado::className(), ['id' => 'entrevistado_id']);
    }
}
