<?php

namespace app\controllers;

use Yii;
use app\models\Entrevistado;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EntrevistadoController implements the CRUD actions for entrevistado model.
 */
class EntrevistadoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new entrevistado model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Entrevistado();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->categoria == 1) {
                return $this->redirect(['/banho-tecnico/quiz', 'id' => $model->id]);
            }
            if($model->categoria == 2) {
                return $this->redirect(['/cme/quiz', 'id' => $model->id]);
            }
            if($model->categoria == 3) {
                return $this->redirect(['/cozinha/quiz', 'id' => $model->id]);
            }
            if($model->categoria == 4) {
                return $this->redirect(['/lavanderia/quiz', 'id' => $model->id]);
            }
            if($model->categoria == 5) {
                return $this->redirect(['/manutencao/quiz', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the entrevistado model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Entrevistado the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Entrevistado::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
