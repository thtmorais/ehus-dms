<?php

namespace app\controllers;

use Yii;
use app\models\Lavanderia;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LavanderiaController implements the CRUD actions for Lavanderia model.
 */
class LavanderiaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new banho-tecnico model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionQuiz($id)
    {
        $model = new Lavanderia();
        $model->entrevistado_id = $id;

        if ($model->load(Yii::$app->request->post())) {
            if($model->q1 == null){
                return $this->render('create', [
                    'model' => $model,
                ]);
            }elseif ($model->transiction == 2 && $model->q3==null) {
                return $this->render('transiction2', [
                    'model' => $model,
                ]);
            }elseif ($model->q3 == null) {
                return $this->render('create2', [
                    'model' => $model,
                ]);
            }elseif ($model->transiction == 3 && $model->q7==null) {
                return $this->render('transiction3', [
                    'model' => $model,
                ]);
            }elseif ($model->q7 == null) {
                return $this->render('create3', [
                    'model' => $model,
                ]);
            }elseif($model->q9a==null){
                return $this->render('create4', [
                    'model' => $model,
                ]);
            }elseif($model->q10==null){
                return $this->render('create5', [
                    'model' => $model,
                ]);
            }elseif ($model->transiction == 4 && $model->q12==null) {
                return $this->render('transiction4', [
                    'model' => $model,
                ]);
            }elseif ($model->q12==null){
                return $this->render('create6', [
                    'model' => $model,
                ]);
            }elseif($model->q14a==null){
                return $this->render('create7', [
                    'model' => $model,
                ]);
            }elseif ($model->transiction == 5 && $model->q15==null) {
                return $this->render('transiction5', [
                    'model' => $model,
                ]);
            }elseif($model->q15==null){
                return $this->render('create8', [
                    'model' => $model,
                ]);
            }elseif ($model->transiction == 6 && $model->q17==null) {
                return $this->render('transiction6', [
                    'model' => $model,
                ]);
            }elseif($model->q17==null){
                return $this->render('create9', [
                    'model' => $model,
                ]);
            }elseif ($model->transiction == 7 && $model->q25==null) {
                return $this->render('transiction7', [
                    'model' => $model,
                ]);
            }elseif($model->q25==null) {
                return $this->render('create10', [
                    'model' => $model,
                ]);
            }elseif($model->save()) {
                return $this->redirect(['/site/obrigado']);
            }
        }else {
            $model->transiction = 1;
            return $this->render('transiction', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Lavanderia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lavanderia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lavanderia::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
