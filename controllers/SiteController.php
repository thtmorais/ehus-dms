<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\BanhoTecnico;
use app\models\Cme;
use app\models\Cozinha;
use app\models\Lavanderia;
use app\models\Manutencao;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays agradecimento.
     *
     * @return string
     */
    public function actionObrigado()
    {
        return $this->render('obrigado');
    }

    /**
     * @return string
     */
    public function actionExportar(){
        $banhoTecnico = new ActiveDataProvider([
            'query' => BanhoTecnico::find(),
        ]);
        $cme = new ActiveDataProvider([
            'query' => Cme::find(),
        ]);
        $cozinha = new ActiveDataProvider([
            'query' => Cozinha::find(),
        ]);
        $lavanderia = new ActiveDataProvider([
            'query' => Lavanderia::find(),
        ]);
        $manutencao = new ActiveDataProvider([
            'query' => Manutencao::find(),
        ]);

        return $this->render('exportar',[
            'banhoTecnico' => $banhoTecnico,
            'cme' => $cme,
            'cozinha' => $cozinha,
            'lavanderia' => $lavanderia,
            'manutnecao' => $manutencao,
        ]);
    }
}
