<?php

use yii\db\Migration;

/**
 * Class m190117_120518_manutencao
 */
class m190117_120518_manutencao extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('manutencao', [
            'q1' => $this->integer()->notNull(),
            'q2' => $this->integer()->notNull(),
            'q3' => $this->integer()->notNull(),
            'q4' => $this->integer()->notNull(),
            'q5' => $this->integer()->notNull(),
            'q6' => $this->integer()->notNull(),
            'q7' => $this->integer()->notNull(),
            'q8' => $this->integer()->notNull(),
            'q9' => $this->integer()->notNull(),
            'q10' => $this->integer()->notNull(),
            'q11a' => $this->integer()->notNull(),
            'q11b' => $this->integer()->notNull(),
            'q11c' => $this->integer()->notNull(),
            'q11d' => $this->integer()->notNull(),
            'q11e' => $this->integer()->notNull(),
            'q11f' => $this->integer()->notNull(),
            'q11g' => $this->integer()->notNull(),
            'q11h' => $this->integer()->notNull(),
            'q11i' => $this->integer()->notNull(),
            'q12' => $this->integer()->notNull(),
            'q13' => $this->integer()->notNull(),
            'q14' => $this->integer()->notNull(),
            'q15' => $this->integer()->notNull(),
            'q16' => $this->integer(),
            'q17a' => $this->integer()->notNull(),
            'q17b' => $this->integer()->notNull(),
            'q17c' => $this->integer()->notNull(),
            'q17d' => $this->integer()->notNull(),
            'q17e' => $this->integer()->notNull(),
            'q17f' => $this->integer()->notNull(),
            'q17g' => $this->integer()->notNull(),
            'q17h' => $this->integer()->notNull(),
            'q17i' => $this->integer()->notNull(),
            'q17j' => $this->integer()->notNull(),
            'q18' => $this->text()->notNull(),
            'q19' => $this->integer()->notNull(),
            'q20' => $this->text()->notNull(),
            'q21' => $this->integer(),
            'q22' => $this->integer()->notNull(),
            'q23' => $this->integer()->notNull(),
            'q24' => $this->integer()->notNull(),
            'q25' => $this->integer()->notNull(),
            'q26' => $this->integer()->notNull(),
            'q27' => $this->integer()->notNull(),
            'q28' => $this->integer()->notNull(),
            'q29' => $this->integer()->notNull(),
            'q30' => $this->text()->notNull(),
            'entrevistado_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'manutencao_entrevistado_fk',
            'manutencao',
            'entrevistado_id',
            'entrevistado',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('manutencao');
    }
}
