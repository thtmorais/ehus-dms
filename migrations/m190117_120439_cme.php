<?php

use yii\db\Migration;

/**
 * Class m190117_120439_cme
 */
class m190117_120439_cme extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cme', [
            'q1' => $this->integer()->notNull(),
            'q2' => $this->integer()->notNull(),
            'q3' => $this->integer()->notNull(),
            'q4' => $this->integer(),
            'q5' => $this->integer()->notNull(),
            'q6' => $this->integer()->notNull(),
            'q7' => $this->integer()->notNull(),
            'q8' => $this->integer()->notNull(),
            'q9' => $this->integer()->notNull(),
            'q10a' => $this->integer()->notNull(),
            'q10b' => $this->integer()->notNull(),
            'q10c' => $this->integer()->notNull(),
            'q10d' => $this->integer()->notNull(),
            'q10e' => $this->integer()->notNull(),
            'q10f' => $this->integer()->notNull(),
            'q10g' => $this->integer()->notNull(),
            'q10h' => $this->integer()->notNull(),
            'q10i' => $this->integer()->notNull(),
            'q11' => $this->integer()->notNull(),
            'q12' => $this->integer()->notNull(),
            'q13' => $this->integer()->notNull(),
            'q14' => $this->integer(),
            'q15a' => $this->integer()->notNull(),
            'q15b' => $this->integer()->notNull(),
            'q15c' => $this->integer()->notNull(),
            'q15d' => $this->integer()->notNull(),
            'q15e' => $this->integer()->notNull(),
            'q15f' => $this->integer()->notNull(),
            'q15g' => $this->integer()->notNull(),
            'q15h' => $this->integer()->notNull(),
            'q15i' => $this->integer()->notNull(),
            'q16' => $this->integer()->notNull(),
            'q17' => $this->integer(),
            'q18' => $this->integer()->notNull(),
            'q19' => $this->integer()->notNull(),
            'q20' => $this->text()->notNull(),
            'q21' => $this->integer()->notNull(),
            'q22' => $this->integer()->notNull(),
            'q23' => $this->integer()->notNull(),
            'q24' => $this->integer()->notNull(),
            'q25' => $this->integer()->notNull(),
            'q26' => $this->text()->notNull(),
            'entrevistado_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'cme_entrevistado_fk',
            'cme',
            'entrevistado_id',
            'entrevistado',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cme');
    }
}
