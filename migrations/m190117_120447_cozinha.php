<?php

use yii\db\Migration;

/**
 * Class m190117_120447_cozinha
 */
class m190117_120447_cozinha extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cozinha', [
            'q1' => $this->integer()->notNull(),
            'q2' => $this->integer()->notNull(),
            'q3' => $this->integer()->notNull(),
            'q4' => $this->integer(),
            'q5' => $this->integer()->notNull(),
            'q6' => $this->integer()->notNull(),
            'q7' => $this->integer()->notNull(),
            'q8' => $this->integer()->notNull(),
            'q9a' => $this->integer()->notNull(),
            'q9b' => $this->integer()->notNull(),
            'q9c' => $this->integer()->notNull(),
            'q9d' => $this->integer()->notNull(),
            'q9e' => $this->integer()->notNull(),
            'q9f' => $this->integer()->notNull(),
            'q9g' => $this->integer()->notNull(),
            'q9h' => $this->integer()->notNull(),
            'q9i' => $this->integer()->notNull(),
            'q10' => $this->integer()->notNull(),
            'q11' => $this->integer()->notNull(),
            'q12' => $this->integer()->notNull(),
            'q13' => $this->integer()->notNull(),
            'q14' => $this->integer()->notNull(),
            'q15' => $this->integer()->notNull(),
            'q16' => $this->integer(),
            'q17a' => $this->integer()->notNull(),
            'q17b' => $this->integer()->notNull(),
            'q17c' => $this->integer()->notNull(),
            'q17d' => $this->integer()->notNull(),
            'q17e' => $this->integer()->notNull(),
            'q17f' => $this->integer()->notNull(),
            'q17g' => $this->integer()->notNull(),
            'q17h' => $this->integer()->notNull(),
            'q17i' => $this->integer()->notNull(),
            'q18' => $this->integer()->notNull(),
            'q19' => $this->integer(),
            'q20' => $this->integer()->notNull(),
            'q21' => $this->integer()->notNull(),
            'q22' => $this->text()->notNull(),
            'q23' => $this->integer()->notNull(),
            'q24' => $this->integer()->notNull(),
            'q25' => $this->integer()->notNull(),
            'q26' => $this->integer()->notNull(),
            'q27' => $this->integer()->notNull(),
            'q28' => $this->text()->notNull(),
            'entrevistado_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'cozinha_entrevistado_fk',
            'cozinha',
            'entrevistado_id',
            'entrevistado',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cozinha');
    }
}
