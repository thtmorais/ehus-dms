<?php

use yii\db\Migration;

/**
 * Class m190117_120428_banho_tecnico
 */
class m190117_120428_banho_tecnico extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('banho_tecnico', [
            'q1' => $this->integer()->notNull(),
            'q2' => $this->integer()->notNull(),
            'q3' => $this->integer()->notNull(),
            'q4' => $this->integer(),
            'q5' => $this->integer()->notNull(),
            'q6' => $this->integer()->notNull(),
            'q7' => $this->integer()->notNull(),
            'q8' => $this->integer()->notNull(),
            'q9a' => $this->integer()->notNull(),
            'q9b' => $this->integer()->notNull(),
            'q9c' => $this->integer()->notNull(),
            'q9d' => $this->integer()->notNull(),
            'q9e' => $this->integer()->notNull(),
            'q9f' => $this->integer()->notNull(),
            'q9g' => $this->integer()->notNull(),
            'q10' => $this->integer()->notNull(),
            'q11' => $this->integer()->notNull(),
            'q12' => $this->integer()->notNull(),
            'q13' => $this->integer(),
            'q14a' => $this->integer()->notNull(),
            'q14b' => $this->integer()->notNull(),
            'q14c' => $this->integer()->notNull(),
            'q14d' => $this->integer()->notNull(),
            'q14e' => $this->integer()->notNull(),
            'q14f' => $this->integer()->notNull(),
            'q14g' => $this->integer()->notNull(),
            'q15' => $this->integer()->notNull(),
            'q16' => $this->integer(),
            'q17' => $this->integer()->notNull(),
            'q18' => $this->integer()->notNull(),
            'q19' => $this->integer()->notNull(),
            'q20' => $this->integer()->notNull(),
            'q21' => $this->integer()->notNull(),
            'q22' => $this->integer()->notNull(),
            'q23' => $this->integer()->notNull(),
            'q24' => $this->integer()->notNull(),
            'q25' => $this->text()->notNull(),
            'entrevistado_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'banho_entrevistado_fk',
            'banho_tecnico',
            'entrevistado_id',
            'entrevistado',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('banho_tecnico');
    }
}
