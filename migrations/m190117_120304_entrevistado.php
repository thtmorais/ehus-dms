<?php

use yii\db\Migration;

/**
 * Class m190117_120504_entrevistado
 */
class m190117_120304_entrevistado extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('entrevistado', [
            'id' => $this->primaryKey(),
            'email' => $this->text()->notNull(),
            'categoria' => $this->integer()->notNull(),
            'aceite' => $this->boolean()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('entrevistado');
    }

}
