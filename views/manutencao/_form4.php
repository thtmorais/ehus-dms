<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <?php echo Progress::widget([
        'percent' => 40.42,
        'barOptions' => ['class' => 'progress-bar-success'],
        'options' => ['class' => 'active progress-striped'],
        'label'=>'40,42'
    ]);?>
    <p></p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'q1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q2')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q3')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q4')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q5')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q6')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q7')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q8')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>4])->label(false) ?>

    <label>Questão 11: Selecione a frequência com que acontecem os problemas de vazamento em:</label>
    <p></p>
    <div style="text-align: left">
        <?php echo $form->field($model, 'q11a')->widget(Select2::classname(), [
            'data' => [1=>'Nunca', 2=>'Ocasionalmente', 3=>'Sempre', 4=>'Não sei/Não se aplica'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q11b')->widget(Select2::classname(), [
            'data' => [1=>'Nunca', 2=>'Ocasionalmente', 3=>'Sempre', 4=>'Não sei/Não se aplica'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q11c')->widget(Select2::classname(), [
            'data' => [1=>'Nunca', 2=>'Ocasionalmente', 3=>'Sempre', 4=>'Não sei/Não se aplica'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q11d')->widget(Select2::classname(), [
            'data' => [1=>'Nunca', 2=>'Ocasionalmente', 3=>'Sempre', 4=>'Não sei/Não se aplica'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q11e')->widget(Select2::classname(), [
            'data' => [1=>'Nunca', 2=>'Ocasionalmente', 3=>'Sempre', 4=>'Não sei/Não se aplica'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q11f')->widget(Select2::classname(), [
            'data' => [1=>'Nunca', 2=>'Ocasionalmente', 3=>'Sempre', 4=>'Não sei/Não se aplica'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q11g')->widget(Select2::classname(), [
            'data' => [1=>'Nunca', 2=>'Ocasionalmente', 3=>'Sempre', 4=>'Não sei/Não se aplica'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q11h')->widget(Select2::classname(), [
            'data' => [1=>'Nunca', 2=>'Ocasionalmente', 3=>'Sempre', 4=>'Não sei/Não se aplica'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q11i')->widget(Select2::classname(), [
            'data' => [1=>'Nunca', 2=>'Ocasionalmente', 3=>'Sempre', 4=>'Não sei/Não se aplica'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
    </div>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
