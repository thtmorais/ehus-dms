<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <?php $form = ActiveForm::begin()?>

    <?= $form->field($model, 'q1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q2')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q3')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q4')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q5')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q6')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q7')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q8')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q11a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11b')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11d')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11f')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11h')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q12')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q13')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q14')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q16')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17b')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17d')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17f')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17h')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17j')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q18')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q19')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>6])->label(false) ?>

    <p class="lead">As próximas duas perguntas têm o objetivo de conhecer as ações de educação ambiental realizadas na
    sua unidade de saúde.</p>
    <p class="lead"> Sua avaliação nos ajudará a conhecer e planejar programas para melhor aproveitamento da água.</p>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
