<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <?php $form = ActiveForm::begin()?>

    <?= $form->field($model, 'q1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q2')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q3')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q4')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q5')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q6')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q7')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q8')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q11a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11b')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11d')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11f')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11h')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q12')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q13')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q14')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q16')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17b')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17d')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17f')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17h')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17j')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q18')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q19')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q20')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q21')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>7])->label(false) ?>

    <p class="lead">Agora, queremos conhecer o perfil de quem respondeu o nosso questionário.</p>
    <p class="lead">Pode ficar tranquilo/a, você não será identificado de maneira nehuma.</p>
    <p class="lead">Só queremos saber quais são as características dos/as servidores que atuam na área de saúde do DF.</p>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
