<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <?php echo Progress::widget([
        'percent' => 80.85,
        'barOptions' => ['class' => 'progress-bar-success'],
        'options' => ['class' => 'active progress-striped'],
        'label'=>'80,85%'
    ]);?>
    <p></p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'q1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q2')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q3')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q4')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q5')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q6')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q7')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q8')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q11a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11b')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11d')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11f')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11h')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q12')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q13')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q14')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q16')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17b')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17d')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17f')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17h')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17j')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q18')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q19')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>6])->label(false) ?>

    <div style="text-align: left">
        <?php echo $form->field($model, 'q20')->widget(Select2::classname(), [
            'data' => [1=>'Não', 2=>'Sim', 3=>'Não sei'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
            'pluginEvents' => [
                "change" => "function() {
                if(parseInt(document.getElementById('manutencao-q20').value) !== 2){
                    document.getElementById('manutencao-q21').value = null;
                return document.getElementById('manutencao-q21').disabled = true;
                } else{
                    return document.getElementById('manutencao-q21').disabled = false;
                }
            }",
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q21')->widget(Select2::classname(), [
            'data' => [1=>'Sem efetividade', 2=>'Razoavelmente efetivas', 3=>'Efetivas'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
    </div>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
