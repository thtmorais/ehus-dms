<?php

$this->title = Yii::$app->name;

?>
<div class="jumbotron">
    <h1>Seja Bem Vindo(a)!</h1>
    <br>
    <p class="lead">Você está prestes a responder a um quiz sobre o uso da água no Sistema de Saúde do DF.
        A sua participação é fundamental para que a gente conheça o sistema, identifique eventuais problemas e saiba
        como agir para solucioná-los. Você, profissional da saúde, que conhece esse sistema por dentro, tem a missão de
        nos prestar informações sobre como ele funciona no dia-a-dia. Está preparado/a?</p>
    <p class="lead">O objetivo é que você responda com sinceridade às perguntas, pois com os resultados nós
        construiremos um protótipo (modelo) de hospital sustentável em tamanho real, que poderá ser visitado na
        Universidade de Brasília. Agora, você dará início à navegação pelo Quiz. Ele é breve! E você poderá acompanhar
        o seu progresso na barra que a aparece na parte superior da tela.</p>
    <p class="lead">E aí? Vamos começar?</p>
    <br><br>
    <a class="btn btn-lg btn-success" href="<?= \yii\helpers\Url::to('@web/entrevistado/create')?>">SIM</a>
    <a class="btn btn-lg btn-danger" onclick="window.close()">NÃO</a>
</div>
