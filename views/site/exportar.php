<?php

use kartik\export\ExportMenu;

$banhoColumns = [
    'q1',
    'q2',
    'q3',
    'q4',
    'q5',
    'q6',
    'q7',
    'q8',
    'q9a', 'q9b', 'q9c', 'q9d', 'q9e', 'q9f', 'q9g',
    'q10',
    'q11',
    'q12',
    'q13',
    'q14a', 'q14b', 'q14c', 'q14d', 'q14e', 'q14f', 'q14g',
    'q15',
    'q16',
    'q17',
    'q18',
    'q19',
    'q20',
    'q21',
    'q22',
    'q23',
    'q24',
    'q25',
];

echo ExportMenu::widget([
    'dataProvider' => $banhoTecnico,
    'columns' => $banhoColumns,
    'fontAwesome' => true,
]);

?>