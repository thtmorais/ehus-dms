<?php

$this->title = Yii::$app->name;

?>
<div class="jumbotron">
    <p class="lead">Muito obrigado por ter respondido ao nosso questionário!</p>
    <p class="lead"> Você contribuiu para aumentar a eficiência do uso da água no sistema de saúde do DF,
        diminuindo o desperdício e contribuindo para um planeta mais sustentável.</p>
    <br><br>
</div>
