<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <?php $form = ActiveForm::begin()?>

    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>1])->label(false) ?>

    <p class="lead">As primeiras duas perguntas têm o objetivo de saber qual é a sua percepção geral sobre a água.</p>
    <p class="lead">Não existe resposta certa ou errada.</p>
    <p class="lead"> Queremos saber o que você pensa sobre o assunto. Vamos lá? </p>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
