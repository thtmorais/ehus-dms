<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */

$this->title = Yii::$app->name;

?>
<div class="banho-tecnico-create">

    <?= $this->render('_form8', [
        'model' => $model,
    ]) ?>

</div>
