<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <?php echo Progress::widget([
        'percent' => 100,
        'barOptions' => ['class' => 'progress-bar-success'],
        'options' => ['class' => 'active progress-striped'],
        'label'=>'100%'
    ]);?>
    <p></p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'q1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q2')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q3')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q4')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q5')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q6')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q7')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q8')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q9b')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q9d')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q9f')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q9h')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q11')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q12')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q13')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q14')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q16')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q17a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17b')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q17c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17d')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q17e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q17f')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q17g')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q17h')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q17i')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q18')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q19')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q20')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q21')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q22')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q23')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q24')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q25')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q26')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q27')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>8])->label(false) ?>

    <div style="text-align: left">
        <?= $form->field($model, 'q28')->textarea(array('rows'=>10,'cols'=>5)); ?>
        <p></p>
    </div>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
