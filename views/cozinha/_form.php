<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <?php echo Progress::widget([
        'percent' => 4.54,
        'barOptions' => ['class' => 'progress-bar-success'],
        'options' => ['class' => 'active progress-striped'],
        'label'=>'4,54%'
    ]);?>
    <p></p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>2])->label(false)?>

    <?php echo $form->field($model, 'q1')->widget(Select2::classname(), [
        'data' => [1=>'Concorda Totalmente', 2=>'Concorda em parte', 3=>'Discorda Totalmente'],
        'options' => ['placeholder' => 'Selecione sua resposta...'],
        'hideSearch' => true,
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    <p></p>
    <?php echo $form->field($model, 'q2')->widget(Select2::classname(), [
        'data' => [1=>'Pouca ou nenhuma importância', 2=>'Importância razoável', 3=>'Muita importância'],
        'options' => ['placeholder' => 'Selecione sua resposta...'],
        'hideSearch' => true,
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    <p></p>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
