<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Entrevistado */

$this->title = Yii::$app->name;


?>

<div class="entrevistado-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
