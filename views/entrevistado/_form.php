<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\Entrevistado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <h3>Para começar, insira seu e-mail e escolha sua categoria.</h3>

    <?php echo Progress::widget([
        'percent' => 0,
        'barOptions' => ['class' => 'progress-bar-success'],
        'options' => ['class' => 'active progress-striped'],
        'label'=>'0%'
    ]);?>
    <p></p>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'categoria')->widget(Select2::classname(), [
        'data' => [1 => 'Banho Técnico', 2 => 'CME', 3 => 'Cozinha Hospitalar', 4 => 'Lavanderia', 5 => 'Manutenção Hidráulica'],
        'options' => ['placeholder' => 'Selecione sua categoria...'],
        'hideSearch' => true,
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>

    <?=  $form->field($model, 'aceite',
    ['options' => ['tag' => 'span'],
        'template' => "{input}"
    ])->checkbox(['checked' => false, 'required' => true]); ?>

    <?= Html::a('<i class="fas fa-paperclip"></i>',Url::to('@web/docs/TCLE.pdf', true)) ?>

    <br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'ENVIAR' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
