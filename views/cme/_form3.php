<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <?php echo Progress::widget([
        'percent' => 21.42,
        'barOptions' => ['class' => 'progress-bar-success'],
        'options' => ['class' => 'active progress-striped'],
        'label'=>'21,42%'
    ]);?>
    <p></p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'q1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q2')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q3')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q4')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q5')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q6')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>4])->label(false) ?>

    <?php echo $form->field($model, 'q7')->widget(Select2::classname(), [
        'data' => [1=>'Péssimo', 2=>'Ruim', 3=>'Regular', 4=>'Bom', 5=>'Ótimo'],
        'options' => ['placeholder' => 'Selecione sua resposta...'],
        'hideSearch' => true,
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    <p></p>
    <?php echo $form->field($model, 'q8')->widget(Select2::classname(), [
        'data' => [1=>'Péssimo', 2=>'Ruim', 3=>'Regular', 4=>'Bom', 5=>'Ótimo'],
        'options' => ['placeholder' => 'Selecione sua resposta...'],
        'hideSearch' => true,
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    <p></p>
    <?php echo $form->field($model, 'q9')->widget(Select2::classname(), [
        'data' => [1=>'Não', 2=>'Sim', 3=>'Não sei'],
        'options' => ['placeholder' => 'Selecione sua resposta...'],
        'hideSearch' => true,
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    <p></p>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
