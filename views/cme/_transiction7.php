<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <?php $form = ActiveForm::begin()?>

    <?= $form->field($model, 'q1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q2')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q3')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q4')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q5')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q6')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q7')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q8')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q8')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10b')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10d')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10f')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10h')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q12')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q13')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q14')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15b')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15d')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15f')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15h')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q16')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q17')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q18')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q19')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q20')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q21')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q22')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q23')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q24')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q25')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>8])->label(false) ?>

    <p class="lead">Para finalizar, queremos saber o que você tem a nos dizer.</p>
    <p class="lead">Deixe sugestões que você daria para melhorar o uso da água na sua unidade de saúde.</p>
    <p class="lead">Suas sugestões são fundamentais e serão levadas em conta na adoção de medidas para tornar mais eficiente nosso sistema hidríco.</p>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
