<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <?php echo Progress::widget([
        'percent' => 97.61,
        'barOptions' => ['class' => 'progress-bar-success'],
        'options' => ['class' => 'active progress-striped'],
        'label'=>'97,61%'
    ]);?>
    <p></p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'q1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q2')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q3')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q4')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q5')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q6')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q7')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q8')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q8')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10b')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10d')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10f')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10h')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q12')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q13')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q14')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15b')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15d')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15f')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15h')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q16')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q17')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>7])->label(false) ?>

    <div style="text-align: left">
        <?php echo $form->field($model, 'q18')->widget(Select2::classname(), [
            'data' => [1=>'Servidor público efetivo', 2=>'Servidor público temporário', 3=>'Prestador de serviço terceirizado', 4=>'Outro tipo de vínculo'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q19')->widget(Select2::classname(), [
            'data' => [1=>'Menos de 40 horas', 2=>'De 40 horas a 44 horas', 3=>'Mais de 44 horas'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q20')->widget(Select2::classname(), [
            'data' => [1=>'Fundamental incompleto', 2=>'Ensino fundamental completo', 3=>'Ensino Fundamental completo com formação técnica', 4=>'Ensino Médio completo', 5=>'Ensino Médio completo com formação técnica', 6=>'Superior completo', 7=>'Pós-graduação'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q21')->widget(Select2::classname(), [
            'data' => [0=>'Masculino', 1=>'Feminino'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q22')->widget(Select2::classname(), [
            'data' => [1=>'Branca', 2=>'Preta', 3=>'Parda', 4=>'Indígena', 5=>'Amarela'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q23')->widget(Select2::classname(), [
            'data' => [1=>'Até dois salários mínimos (até R$ 1.908,00)', 2=>'De dois a quatro salários mínios (de R$ 1.908,01 a R$ 3.816,00)', 3=>'De quatro a oito salários mínimos (de R$ 3.816,01 a R$ 7.632,00)', 4=>'De oito a doze salários mínimos (de R$ 7.632,01 a R$ 11.448,00)', 5=>'Acima de doze salários minimos (caima de R$ 11.448,00)'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q24')->widget(Select2::classname(), [
            'data' => [1=>'Menos de 1 ano', 2=>'De 1 a 5 anos', 3=>'De 6 a 10 anos', 4=>'De 11 a 20 anos', 5=>'Mais de 20 anos'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q25')->widget(Select2::classname(), [
            'data' => [1=>'Menos de 1 ano', 2=>'De 1 a 5 anos', 3=>'De 6 a 10 anos', 4=>'De 11 a 20 anos', 5=>'Mais de 20 anos'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
    </div>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
