<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <?php echo Progress::widget([
        'percent' => 78.57,
        'barOptions' => ['class' => 'progress-bar-success'],
        'options' => ['class' => 'active progress-striped'],
        'label'=>'78,57%'
    ]);?>
    <p></p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'q1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q2')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q3')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q4')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q5')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q6')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q7')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q8')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q8')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10b')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10d')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10f')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10h')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q10i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q11')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q12')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q13')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q14')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15b')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15d')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15f')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q15h')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q15i')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>6])->label(false) ?>


    <div style="text-align: left">
        <?php echo $form->field($model, 'q16')->widget(Select2::classname(), [
            'data' => [1=>'Não', 2=>'Sim', 3=>'Não sei'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
            'pluginEvents' => [
                "change" => "function() {
                if(parseInt(document.getElementById('cme-q16').value) !== 2){
                    document.getElementById('cme-q17').value = null;
                return document.getElementById('cme-q17').disabled = true;
                } else{
                    return document.getElementById('cme-q17').disabled = false;
                }
            }",
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q17')->widget(Select2::classname(), [
            'data' => [1=>'Sem efetividade', 2=>'Razoavelmente efetivas', 3=>'Efetivas'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
    </div>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
