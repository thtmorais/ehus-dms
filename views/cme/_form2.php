<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="jumbotron">

    <?php echo Progress::widget([
        'percent' => 14.28,
        'barOptions' => ['class' => 'progress-bar-success'],
        'options' => ['class' => 'active progress-striped'],
        'label'=>'14,28%'
    ]);?>
    <p></p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'q1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q2')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>3])->label(false) ?>

    <?php echo $form->field($model, 'q3')->widget(Select2::classname(), [
        'data' => [1=>'Não', 2=>'Sim', 3=>'Não sei'],
        'options' => ['placeholder' => 'Selecione sua resposta...'],
        'hideSearch' => true,
        'pluginOptions' => [
            'allowClear' => true,
        ],
        'pluginEvents' => [
            "change" => "function() {
                if(parseInt(document.getElementById('cme-q3').value) !== 2){
                    document.getElementById('cme-q4').value = null;
                return document.getElementById('cme-q4').disabled = true;
                } else{
                    return document.getElementById('cme-q4').disabled = false;
                }
            }",
        ],
    ]);?>
    <p></p>
    <?php echo $form->field($model, 'q4')->widget(Select2::classname(), [
        'data' => [1=>'Algumas horas', 2=>'Um dia', 3=>'Dois dias', 4=>'Três dias', 5=>'Quatro dias ou mais', 6=>'Não sei'],
        'options' => ['placeholder' => 'Selecione sua resposta...'],
        'hideSearch' => true,
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    <p></p>
    <?php echo $form->field($model, 'q5')->widget(Select2::classname(), [
        'data' => [1=>'Não', 2=>'Sim', 3=>'Não sei'],
        'options' => ['placeholder' => 'Selecione sua resposta...'],
        'hideSearch' => true,
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    <p></p>
    <?php echo $form->field($model, 'q6')->widget(Select2::classname(), [
        'data' => [1=>'Não', 2=>'Sim', 3=>'Não sei'],
        'options' => ['placeholder' => 'Selecione sua resposta...'],
        'hideSearch' => true,
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    <p></p>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
