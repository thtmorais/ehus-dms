<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\BanhoTecnico */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jumbotron">

    <?php echo Progress::widget([
        'percent' => 78.57,
        'barOptions' => ['class' => 'progress-bar-success'],
        'options' => ['class' => 'active progress-striped'],
        'label'=>'78,57%'
    ]);?>
    <p></p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'q1')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q2')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q3')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q4')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q5')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q6')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q7')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q8')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q9b')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q9d')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q9f')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q9g')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q9h')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q10')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q11')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q12')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q13')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q14a')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q14b')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q14c')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q14d')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q14e')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q14f')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q14g')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q14h')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q14i')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'q14j')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'q14k')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'transiction')->hiddenInput(['value'=>6])->label(false) ?>


    <div style="text-align: left">
        <?php echo $form->field($model, 'q15')->widget(Select2::classname(), [
            'data' => [1=>'Não', 2=>'Sim', 3=>'Não sei'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
            'pluginEvents' => [
                "change" => "function() {
                if(parseInt(document.getElementById('lavanderia-q15').value) !== 2){
                    document.getElementById('lavanderia-q16').value = null;
                return document.getElementById('lavanderia-q16').disabled = true;
                } else{
                    return document.getElementById('lavanderia-q16').disabled = false;
                }
            }",
            ],
        ]);?>
        <p></p>
        <?php echo $form->field($model, 'q16')->widget(Select2::classname(), [
            'data' => [1=>'Sem efetividade', 2=>'Razoavelmente efetivas', 3=>'Efetivas'],
            'options' => ['placeholder' => 'Selecione sua resposta...'],
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        <p></p>
    </div>
    <br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Avançar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
